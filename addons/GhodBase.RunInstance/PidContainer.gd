tool
extends HBoxContainer

var pidId = -1
var output = []
var options = []

func _on_btnKill_pressed():
	if pidId > 0:
		OS.kill(pidId)
		queue_free()


func _on_btnViewOutput_pressed():
	var outputDialogInstance = preload("res://addons/GhodBase.RunInstance/OutputDialog.tscn").instance()
	outputDialogInstance.get_node("lblOutput").text = String(output)
	get_tree().root.add_child(outputDialogInstance)
	outputDialogInstance.show()


func _on_btnReRun_pressed():
	if pidId > 0:
		OS.kill(pidId)
		var pid = OS.execute(OS.get_executable_path(), options, false, output)
		pidId = pid
		$lblPid.text = String(pid)
