tool
extends ConfirmationDialog

var current_path = ""
var newInstanceDocker

func _on_btnChangePath_pressed():
	var SelectScene = preload("res://addons/GhodBase.RunInstance/SelectScene.tscn").instance()
	SelectScene.newInstanceDocker = $"."
	SelectScene.current_dir = ProjectSettings.globalize_path("res://")
	get_tree().root.add_child(SelectScene)
	SelectScene.popup()


func _on_OptionsDialog_confirmed():
	var pidCountainerInstance = preload("res://addons/GhodBase.RunInstance/PidContainer.tscn").instance()
	var options = ["--path", $VBoxContainer/Container/txtPath.text]
	
	if $VBoxContainer/chkDebugCollision.pressed:
		options.append("--debug-collisions")
	if $VBoxContainer/chkDebugNavigation.pressed:
		options.append("--debug-navigation")
	if $VBoxContainer/chkCommandLineDebug.pressed:
		options.append("--debug")
	if $VBoxContainer/chkOnTop.pressed:
		options.append("--always-on-top")
	
	options.append(current_path)
	var pid = OS.execute(OS.get_executable_path(), options, false, pidCountainerInstance.output)
	pidCountainerInstance.pidId = pid
	pidCountainerInstance.options = options
	pidCountainerInstance.get_node("lblPid").text = String(pid)
	newInstanceDocker.add_child(pidCountainerInstance)
