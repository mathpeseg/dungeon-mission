tool
extends Control


func _on_Button_pressed():
	var SelectScene = preload("res://addons/GhodBase.RunInstance/SelectScene.tscn").instance()
	SelectScene.newInstanceDocker = $"."
	SelectScene.current_dir = ProjectSettings.globalize_path("res://")
	get_tree().root.add_child(SelectScene)
	SelectScene.popup()
