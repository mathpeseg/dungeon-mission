extends Node2D




onready var vision = $vision

func _ready():
	vision.connect("area_entered", self, "_vision_area_entered")
	vision.connect("area_exited", self, "_vision_area_exited")
	

func _vision_area_entered(body):
	if body.get_parent() != self and body.name != "vision":
		print(body.name," of ",  body.get_parent().name, " entered in ", self.name, " vision")
	
func _vision_area_exited(body):
	print(body.name," of ",  body.get_parent().name, " exited of ", self.name, " vision")
	
	
	