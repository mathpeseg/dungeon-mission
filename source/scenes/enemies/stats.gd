extends Node

export var _hp  = 10
export var _mp  = 10
export var _max_hp  = 10
export var _max_mp  = 10
export var _str = 1
export var _def = 1

func _process(delta):
	var value = float(_hp)/float(_max_hp)*10
	if value > 2:
		get_node("hp_bar").value = value
	else: get_node("hp_bar").value = 2

	if _hp <= 0:
		get_parent().queue_free()