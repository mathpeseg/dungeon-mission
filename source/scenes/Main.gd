extends Node

onready var fps_label = $fps_label
func _process(delta):
	fps_label.text = str(Engine.get_frames_per_second())