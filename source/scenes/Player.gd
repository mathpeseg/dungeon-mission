extends Node2D
onready var player_body		 = $BODY

onready var raycast_up		 = $raycasts/UP
onready var raycast_down	 = $raycasts/DOWN
onready var raycast_left	 = $raycasts/LEFT
onready var raycast_right	 = $raycasts/RIGHT
onready var buttons = {"up":$screen_buttons/UP, "down":$screen_buttons/DOWN, "left":$screen_buttons/LEFT, "right":$screen_buttons/RIGHT}
var grid_size = Vector2(16,16)

onready var raycasts	 = [raycast_up, raycast_down, raycast_left, raycast_right]

func _ready():
	randomize()
	for ray in raycasts:
		ray.add_exception(player_body)
	
	buttons.left.connect("pressed", self, "_left_pressed")
	buttons.right.connect("pressed", self, "_right_pressed")
	buttons.up.connect("pressed", self, "_up_pressed")
	buttons.down.connect("pressed", self, "_down_pressed")
	

func _left_pressed():
	var this_direction_raycast = raycast_left
	
	if !this_direction_raycast.is_colliding():
		self.position.x -= grid_size.x
	else:
		if this_direction_raycast.get_collider().get_parent().is_in_group("enemy") && this_direction_raycast.get_collider().name == "BODY":
			var collider_stats = this_direction_raycast.get_collider().get_parent().get_node('stats')
			collider_stats._hp -= damage_calculation(self.get_node("stats")._str, collider_stats._def)
		elif !this_direction_raycast.get_collider().is_in_group("solid"):
			self.position.x -= grid_size.x
		
func _right_pressed():
	var this_direction_raycast = raycast_right
	
	if !this_direction_raycast.is_colliding():
		self.position.x += grid_size.x 
	else:
		if this_direction_raycast.get_collider().get_parent().is_in_group("enemy") && this_direction_raycast.get_collider().name == "BODY":
			var collider_stats = this_direction_raycast.get_collider().get_parent().get_node('stats')
			collider_stats._hp -= damage_calculation(self.get_node("stats")._str, collider_stats._def)
		elif !this_direction_raycast.get_collider().is_in_group("solid"):
			self.position.x += grid_size.x 


func _up_pressed():
	var this_direction_raycast = raycast_up
	
	if !this_direction_raycast.is_colliding():
		self.position.y -= grid_size.y
	else:
		if this_direction_raycast.get_collider().get_parent().is_in_group("enemy") && this_direction_raycast.get_collider().name == "BODY" :
			var collider_stats = this_direction_raycast.get_collider().get_parent().get_node('stats')
			collider_stats._hp -= damage_calculation(self.get_node("stats")._str, collider_stats._def)
		elif !this_direction_raycast.get_collider().is_in_group("solid"):
			self.position.y -= grid_size.y
		
		
func _down_pressed():
	var this_direction_raycast = raycast_down
	
	if !this_direction_raycast.is_colliding():
		self.position.y += grid_size.y 
	else:
		if this_direction_raycast.get_collider().get_parent().is_in_group("enemy") && this_direction_raycast.get_collider().name == "BODY":
			var collider_stats = this_direction_raycast.get_collider().get_parent().get_node('stats')
			collider_stats._hp -= damage_calculation(self.get_node("stats")._str, collider_stats._def)
		elif !this_direction_raycast.get_collider().is_in_group("solid"):
			self.position.y += grid_size.y 


func _input(event):
	if Input.is_action_just_pressed("ui_right"):
		_right_pressed()
	if Input.is_action_just_pressed("ui_left"):
		_left_pressed()
	if Input.is_action_just_pressed("ui_up"):
		_up_pressed()
	if Input.is_action_just_pressed("ui_down"):
		_down_pressed()
	

func damage_calculation(_str, _def):
	if _str > _def:
		return (_str - randi()%(_def+1))
	else:
		return ((_str - randi()%(_str+1))/2)
		
		
	
	